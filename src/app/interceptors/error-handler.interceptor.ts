import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, of } from "rxjs";
import { catchError } from "rxjs/operators";
import { Router } from '@angular/router';

@Injectable()
export class ErrorHandlerInterceptor implements HttpInterceptor {

  constructor(private router: Router) {}

  /**
   * Interceptor clones the outgoing request and incoming responses and adds extra information to the requests.
   * @param request HttpRequest
   * @param next HttpHandler
   * @returns intercept
   */
  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
      return next.handle(request).pipe(
        catchError(error => {
          this.errorHandler(error);
          return of(error.error);
        }) as any
      );
  }


   /**
   * Errors handling method for handling the errors globally based on the status codes.
   * @param error HttpErrorResponse object
   * @returns error
   */
  private errorHandler(error: HttpErrorResponse): Observable<any> {
    switch (error.status) {
      case 401:
        // if the user is unauthorized it clears the local and session storage and redirect to login page.
        localStorage.clear();
        sessionStorage.clear();
        this.router.navigate(["login"]);
        throw error.error;
        break;
      case 403:
        // implement error handling based on status code
        throw error.error;
        break;
      case 500:
        // implement error handling based on status code
        throw error.error;
        break;
      default:
        throw error.error;
        break;
    }
  }
}
